terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token       = var.sa_key
  zone        = var.zone
  cloud_id    = var.cloud_id
  folder_id   = var.folder_id
}

variable "sa_key" {}
variable "cloud_id" {}
variable "folder_id" {}
variable "service_account_id" {}
variable "network_id" {}
variable "subnet_id" {}
variable "zone" {}

resource "yandex_kubernetes_cluster" "otus_k8s_cluster" {
  name        = "otus-k8s"
  description = "Kubernetes cluster for OTUS course"
  network_id  = var.network_id
  service_account_id      = var.service_account_id
  node_service_account_id = var.service_account_id

  master {
    version      = "1.26"
    public_ip = true
    zonal {
      zone      = var.zone
      subnet_id = var.subnet_id
    }
  }
}

resource "yandex_kubernetes_node_group" "otus_k8s_master_group" {
  cluster_id  = yandex_kubernetes_cluster.otus_k8s_cluster.id
  name        = "otus-k8s-master-group"
  description = "Master node group for OTUS Kubernetes cluster"
  version     = "1.26"

  instance_template {
    platform_id = "standard-v2"

    resources {
      memory = 4
      cores  = 2
    }

    boot_disk {
      size = 30
      type = "network-ssd"
    }

    network_interface {
      subnet_ids = [var.subnet_id]
      nat        = true
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = var.zone
    }
  }
}

resource "yandex_kubernetes_node_group" "otus_k8s_worker_group" {
  cluster_id  = yandex_kubernetes_cluster.otus_k8s_cluster.id
  name        = "otus-k8s-worker-group"
  description = "Worker node group for OTUS Kubernetes cluster"
  version     = "1.26"

  instance_template {
    platform_id = "standard-v2"

    resources {
      memory = 4
      cores  = 2
    }

    boot_disk {
      size = 30
      type = "network-ssd"
    }

    network_interface {
      subnet_ids = [var.subnet_id]
      nat        = true
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    location {
      zone = var.zone
    }
  }
}

